# Audio Manager Package
Plugin to play music and audio cues form anywhere.

## Requirements
* Unity 2020.3 or later.

## Dependencies
* DOTween: ```https://github.com/grashaar/DOTween.git```
* ```com.bourdonvalentin.object-pooling: https://gitlab.com/valentin_bourdon/ObjectPooling.git ```
* ```com.bourdonvalentin.scene-management: https://gitlab.com/valentin_bourdon/SceneManagement.git ```

## Installing the Package
* Add the name of the package (found in package.json) followed by the repository URL to your Unity project's manifest.json or using the package manager in Unity.

```json
{
  "dependencies": {
    "com.bourdonvalentin.audio-manager": "https://gitlab.com/valentin_bourdon/AudioManager.git",
}
```

## License

Initial work made by [UnityTechnologies](https://github.com/UnityTechnologies), provided under the Apache License 2.0, see [COPY](./COPY) file.
All the changes made are released under the MIT License, see [LICENSE](./LICENSE).
