﻿using System;
using UnityEngine;

namespace AudioManager
{

    [CreateAssetMenu(fileName = "AudioSO", menuName = "Audio/AudioSO")]
    public class AudioSO : ScriptableObject
    {
        public bool looping = false;
        [SerializeField] private AudioClipGroup[] _audioClipGroup = default;

        public AudioClip[] GetClips()
        {
            int numberOfAudioClip = _audioClipGroup.Length;
            AudioClip[] resultingAudioClip = new AudioClip[numberOfAudioClip];

            for (int i = 0; i < numberOfAudioClip; i++)
            {
                resultingAudioClip[i] = _audioClipGroup[i].GetNextAudioClip();
            }

            return resultingAudioClip;
        }

        /// <summary>
        /// Represents a group of AudioClips that can be treated as one, and provides automatic randomisation or sequencing based on the <c>SequenceMode</c> value.
        /// </summary>
        [Serializable]
        public class AudioClipGroup
        {
            public SequenceMode sequenceMode = SequenceMode.RandomNoImmediateRepeat;
            public AudioClip[] audioClipDatas;

            private int _nextAudioClipToPlay = -1;
            private int _lastAudioClipPlayed = -1;

            /// <summary>
            /// Chooses the next clip in the sequence, either following the order or randomly.
            /// </summary>
            /// <returns>A reference to an AudioClip</returns>
            public AudioClip GetNextAudioClip()
            {
                // Fast out if there is only one clip to play
                if (audioClipDatas.Length == 1)
                    return audioClipDatas[0];

                if (_nextAudioClipToPlay == -1)
                {
                    // Index needs to be initialised: 0 if Sequential, random if otherwise
                    _nextAudioClipToPlay = (sequenceMode == SequenceMode.Sequential) ? 0 : UnityEngine.Random.Range(0, audioClipDatas.Length);
                }
                else
                {
                    // Select next clip index based on the appropriate SequenceMode
                    switch (sequenceMode)
                    {
                        case SequenceMode.Random:
                            _nextAudioClipToPlay = UnityEngine.Random.Range(0, audioClipDatas.Length);
                            break;

                        case SequenceMode.RandomNoImmediateRepeat:
                            do
                            {
                                _nextAudioClipToPlay = UnityEngine.Random.Range(0, audioClipDatas.Length);
                            } while (_nextAudioClipToPlay == _lastAudioClipPlayed);
                            break;

                        case SequenceMode.Sequential:
                            _nextAudioClipToPlay = (int)Mathf.Repeat(++_nextAudioClipToPlay, audioClipDatas.Length);
                            break;
                    }
                }

                _lastAudioClipPlayed = _nextAudioClipToPlay;

                return audioClipDatas[_nextAudioClipToPlay];
            }

            public enum SequenceMode
            {
                Random,
                RandomNoImmediateRepeat,
                Sequential,
            }
        }
    }
}
