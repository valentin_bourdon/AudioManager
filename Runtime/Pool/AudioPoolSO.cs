using UnityEngine;
using ObjectPooling.Pool;
using ObjectPooling.Factory;

namespace AudioManager
{
	[CreateAssetMenu(fileName = "NewSoundEmitterPool", menuName = "Pool/SoundEmitter Pool")]
	public class AudioPoolSO : ComponentPoolSO<SoundEmitter>
	{
		[SerializeField]
		private AudioFactorySO _factory;

		public override IFactory<SoundEmitter> Factory
		{
			get
			{
				return _factory;
			}
			set
			{
				_factory = value as AudioFactorySO;
			}
		}
	}
}