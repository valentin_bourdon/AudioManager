using UnityEngine;
using ScriptableObjectWorkflow.Events;
using AudioManager.Events;

namespace AudioManager
{
	public class MusicPlayer : MonoBehaviour
	{
		[SerializeField] private VoidEventChannelSO _onSceneReady = default;
		[SerializeField] private AudioCueEventChannelSO _playMusicOn = default;
		[SerializeField] private AudioSO _audioSO = default;
		[SerializeField] private AudioConfigurationSO _audioConfig = default;

		private void OnEnable()
		{
			_onSceneReady.OnEventRaised += PlayMusic;
		}

		private void OnDisable()
		{
			_onSceneReady.OnEventRaised -= PlayMusic;
		}

		private void PlayMusic()
		{
			if (_audioSO != null)
				_playMusicOn.RaisePlayEvent(_audioSO, _audioConfig);
		}
	}
}
